package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard


class FlashcardSetDetailActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)
        val adapterCustom = FlashcardAdapter(Flashcard.getHardcodedFlashcards())
        val recycler2 = findViewById<RecyclerView>(R.id.recycler2)
        recycler2.adapter = adapterCustom
        val button = findViewById<Button>(R.id.my_button)
        button.setOnClickListener {
            adapterCustom.updateList(Flashcard("new term","new definition"))
        }
    }
}
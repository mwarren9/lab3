package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R

import cs.mad.flashcards.entities.Flashcard


class FlashcardAdapter(input: List<Flashcard>):
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val myData2 = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView2 = view.findViewById<TextView>(R.id.my_text2)
    }

    override fun onCreateViewHolder(viewGroup2: ViewGroup,viewType: Int): FlashcardAdapter.ViewHolder {
        return FlashcardAdapter.ViewHolder(LayoutInflater.from(viewGroup2.context).inflate(R.layout.item_flashcard, viewGroup2, false))
    }

    override fun onBindViewHolder(holder: FlashcardAdapter.ViewHolder, position: Int) {
        holder.textView2.text = myData2[position].term
    }

    override fun getItemCount(): Int {
        return myData2.size
    }
    fun updateList(input:Flashcard){
        myData2.add(input)
        notifyDataSetChanged()
    }
}
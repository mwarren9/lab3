package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetAdapter(input: List<FlashcardSet>) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    private val myData = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById<TextView>(R.id.my_text)
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        /*
            this is written for you but understand what is happening here
            the layout for an individual item is being inflated
            the inflated layout is passed to view holder for storage

            THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
         */
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set, viewGroup, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = myData[position].title
        holder.itemView.setOnClickListener {
            holder.itemView.context.startActivity(Intent(holder.itemView.context, FlashcardSetDetailActivity::class.java))
        }
    }
    override fun getItemCount(): Int {
        // return the size of the data set
        return myData.size
    }
    fun updateList(input: FlashcardSet){
        myData.add(input)
        notifyDataSetChanged()
    }

}
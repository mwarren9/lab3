//
//  Custom Alert View Controller Scene.swift
//  CS422L
//
//  Created by Student Account  on 2/4/22.
//

import UIKit
class FlashcardStudyController: UIViewController {
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet var StudyView: UIView!
    
    var flashcardVC:DetailViewController
    
    @IBOutlet var TermTextField: UITextField!
    @IBOutlet var DefinitionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TermTextField.text = flashcardVC.set[flashcardVC.selectedIndex].term
        DefinitionTextField.text = flashcardVC.set[flashcardVC.selectedIndex].definition
    }
    @IBAction func DoneButton(_ sender: Any) {
        self.dismiss(animated:true,completion:{})
    }
    @IBAction func DeleteButton(_ sender: Any) {
        self.dismiss(animated:true,completion:{})
    }
    
    
}

//
//  ViewController.swift
//  CS422L
//
//  Created by Student Account  on 1/28/22.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    var set: [Flashcard] = [Flashcard]()
    var selectedIndex: Int = 0
    var clickedCount = 1
    
    @IBOutlet weak var FlashcardTableView: UITableView!
    @IBOutlet var FlashcardLabel: UILabel!
    
    @objc func labelClicked(_ sender: Any){
        let alertController = UIAlertController(title: set[selectedIndex].term, message: set[selectedIndex].definition, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Edit", style: .default))
        
        self.present(alertController,animated: true,completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return set.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass2 = Flashcard()
        FlashcardTableView.delegate = self
        FlashcardTableView.dataSource = self
        set = Flashcard.getHardCodedFlashcards()
        
        //user interaction
        FlashcardLabel.isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(labelClicked(_:)))
        FlashcardLabel.addGestureRecognizer(gestureRecognizer)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        self.FlashcardTableView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer){
        let p = longPressGesture.location(in: self.FlashcardTableView)
        let indexPath = self.FlashcardTableView.indexPathForRow(at: p)
        
        if longPressGesture.state == UIGestureRecognizer.State.began{
            selectedIndex = indexPath?.row ?? 0
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let alertVC = sb.instantiateViewController(withIdentifier: "FlashcardStudyController") as! FlashcardStudyController
            alertVC.flashcardVC = self
            alertVC.modalPresentationStyle = .overCurrentContext
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetCell2", for: indexPath) as! TableViewCell
        cell.textLabel?.text = set[indexPath.row].term
        return cell
    }
    @IBAction func Add(_ sender: Any) {
        var fc1 = Flashcard()
        fc1.term = "New Term"
        fc1.definition = "New Definition"
        set.append(fc1)
        FlashcardTableView.reloadData()
    }
    
    
    
    
}

//
//  UIAlertControllerViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/4/22.
//

import UIKit

class UIAlertControllerViewController: UIAlertController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let alert = UIAlertController(title: "My Alert", message: "This is an alert.", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default Action"), style: .default, handler: {_ in}))
        self.present(alert, animated:true,completion:nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    var sets: [FlashcardSet] = [FlashcardSet]()
    
    @IBOutlet weak var FlashcardCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = FlashcardSet()
        FlashcardCollection.delegate = self
        FlashcardCollection
            .dataSource = self
        sets = FlashcardSet.getHardCodedCollection()
        //connect hard coded collection to sets
    
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "GoToDetail", sender:self)
    }
    @IBAction func flashcardSetButton(_ sender: Any) {
        var fc2 = FlashcardSet()
        fc2.title = "New Title"
        sets.append(fc2)
        FlashcardCollection.reloadData()
        }
}

